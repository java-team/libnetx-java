#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>
TAR=../libnetx-java_$2.orig.tar.gz
DIR=libnetx-java-$2.orig

# clean up the upstream tarball
unzip $3
mv netx-$2 $DIR
GZIP=--best tar -c -z -f $TAR --exclude '*/javadoc/*' --exclude '*.jar' $DIR
rm -rf $3 $DIR

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir
  echo "moved $TAR to $origDir"
fi
